
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import home from './src/home'
import detail from './src/detail'


const HomeStack = createStackNavigator({
  home: {
    screen: home,
    
  },
  detail: {
    screen:detail
  }
})

const AppContainer = createAppContainer(HomeStack)
export default class App extends React.Component{
  render() {
    return (
      <AppContainer/>
    )
  }
}


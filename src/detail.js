import React from 'react'
import { StyleSheet, View ,FlatList,Text,TouchableOpacity} from 'react-native'

class detail extends React.Component {
    constructor() {
        super();
        this.state = {
            disData:[],
            counter: 0
            
        }
    }
 
    componentDidMount() {

        const { navigation } = this.props;
        var JsonData = navigation.getParam('item')
        this.setState({disData:JsonData})
    }


    render() {
        const { navigation } = this.props;
        var JsonData = navigation.getParam('item')
        return (
            <View style={styles.mainContainer}>
             <View style={{ flex: 1 ,margin:15}}>
                    <Text>Titel : {JsonData.title}</Text>
                    <Text>Url : {JsonData.url}</Text>
                    <Text>Created_at : {JsonData.created_at}</Text>
                    <Text>Author : {JsonData.author}</Text>
            </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        // backgroundColor:'red'
    }
})
export default detail;
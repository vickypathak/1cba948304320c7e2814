import React from 'react'
import { StyleSheet, View ,FlatList,Text,TouchableOpacity} from 'react-native'

class home extends React.Component {
    constructor() {
        super();
        this.state = {
            arrData:[],
            disData:[],
            counter: 0
            
        }
    }
 
    componentDidMount() {
        this.getDataFromApi()
        setInterval(() => {
            this.setCounter()
        },10000)
        
    }
  
    getDataFromApi = () => {
       // console.log("counter..",this.state.counter)
        fetch(`https://hn.algolia.com/api/v1/search_by_date?tags=story&page=${this.state.counter}`)
            .then((res) => res.json())
            .then((res) => {
                // console.log("=========",res.hits)
                
               res.hits.map((item, index) => {
                this.state.arrData.push(item)
                })
                this.setState({ arrData:this.state.arrData})
                
                
            })
    }

    setCounter = () => {
        this.setState({
            counter: this.state.counter + 1
        })
        this.getDataFromApi()
    }
    renderApiData = ({item} ) => {
       // console.log(item)
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity style={{margin:15,}} onPress={()=>this.props.navigation.navigate('detail',{item:item})}>
                    <Text>Titel : {item.title}</Text>
                    <Text>Url : { item.url}</Text>
                    <Text>Created_at : {item.created_at}</Text>
                    <Text>Author : { item.author}</Text>
                </TouchableOpacity>
                

            </View>
        )
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <FlatList
                     data={this.state.arrData}
                    renderItem={this.renderApiData}
                    keyExtractor={(item, index) => index.toString()}
                    onScrollEndDrag={()=>{this.setCounter()}}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        // backgroundColor:'red'
    }
})
export default home;